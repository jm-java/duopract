import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class Main {
    static String[] works = {"Собирать молотки", "Пилить ручки", "Делать бойки", "Упаковывать молотки"};
    static String[] employee = {"Васильев Василий", "Ходжахов Эмиль", "Лысенко Семён", "Солнцев Гоген"};
    public static ArrayList<String> rawHammers = new ArrayList<>();
    public static ArrayList<String> packedHammers = new ArrayList<>();
    public static void main(String[] args) {
        factoryStatus();
    }
    public static void getWork() {
        Random random = new Random();
        int idx = random.nextInt(works.length);
        System.out.println("Ваша работа на эту смену: " + works[idx]);
    }

    public static void factoryStatus() {
        int hours = Integer.parseInt(new SimpleDateFormat("HH").format(Calendar.getInstance().getTime()));
        if(hours < 8 || hours > 22) {
            System.out.println("Завод закрыт! Приходите завтра");
        }
        else {
            System.out.println("Рабочие работают, завод работает!");
        }
    }
    public static void addHammers(int index) {
        for (int i = 1; i < index; i++) {
            rawHammers.add("МОЛОТОК " + index);
        }

    }

    public static String getRandomEmployee() {
        return employee[new Random().nextInt(employee.length)];
    }
    public static String getPackedHammer() {
        return packedHammers.get(new Random().nextInt(employee.length));
    }

    public static String getRawHammer() {
        return rawHammers.get(new Random().nextInt(employee.length));
    }

    public static String sealHammer(int index) {
        String hammer = rawHammers.get(index);
        packedHammers.add(hammer);
        rawHammers.remove(index);
    }

}